# 👋🏾🐻 Heya 

<a href="#"> <!-- https://blog.phenjara.de/ -->
    <img src="https://gitlab.com/sythelux/sythelux/-/raw/main/imgs/wordpress.svg" alt="wordpress" style="vertical-align:top margin:6px 4px">
</a>
<br />

**🔭 Interests**

<p align="center">
    <img src="https://gitlab.com/sythelux/sythelux/-/raw/main/imgs/gamedev.svg" alt="gamedev" style="vertical-align:top margin:6px 4px">
    <img src="https://gitlab.com/sythelux/sythelux/-/raw/main/imgs/vr.svg" alt="vr" style="vertical-align:top margin:6px 4px">
    <img src="https://gitlab.com/sythelux/sythelux/-/raw/main/imgs/web.svg" alt="web" style="vertical-align:top margin:6px 4px">
    <img src="https://gitlab.com/sythelux/sythelux/-/raw/main/imgs/iot.svg" alt="iot" style="vertical-align:top margin:6px 4px">
    <img src="https://gitlab.com/sythelux/sythelux/-/raw/main/imgs/tools.svg" alt="tools" style="vertical-align:top margin:6px 4px">
    <img src="https://gitlab.com/sythelux/sythelux/-/raw/main/imgs/datascience.svg" alt="datascience" style="vertical-align:top margin:6px 4px">
    <img src="https://gitlab.com/sythelux/sythelux/-/raw/main/imgs/docker.svg" alt="docker" style="vertical-align:top margin:6px 4px">
</p>
<br />

**⚡ Engines / IDEs of Choice**

<p align="left">
    <img src="https://gitlab.com/sythelux/sythelux/-/raw/main/imgs/godot.svg" alt="godot" style="vertical-align:top margin:6px 4px">
    <img src="https://gitlab.com/sythelux/sythelux/-/raw/main/imgs/unity.svg" alt="unity" style="vertical-align:top margin:6px 4px">
    <img src="https://gitlab.com/sythelux/sythelux/-/raw/main/imgs/jetbrains_clion.svg" alt="jetbrains clion" style="vertical-align:top margin:6px 4px">
    <img src="https://gitlab.com/sythelux/sythelux/-/raw/main/imgs/jetbrains_rider.svg" alt="jetbrains rider" style="vertical-align:top margin:6px 4px">
</p>
<br />

**💬 Languages of Choice**

<p align="right">
    <img src="https://gitlab.com/sythelux/sythelux/-/raw/main/imgs/bash.svg" alt="bash" style="vertical-align:top margin:6px 4px">
    <img src="https://gitlab.com/sythelux/sythelux/-/raw/main/imgs/csharp_dotnet.svg" alt="csharp_dotnet" style="vertical-align:top margin:6px 4px">
    <img src="https://gitlab.com/sythelux/sythelux/-/raw/main/imgs/rust.svg" alt="rust" style="vertical-align:top margin:6px 4px">
    <img src="https://gitlab.com/sythelux/sythelux/-/raw/main/imgs/python.svg" alt="python" style="vertical-align:top margin:6px 4px">
</p>
<br />

**✨ Stats**

![sythelux's gitlab stats](https://gitlab-readme-stats-flax.vercel.app/api?username=sythelux&show_icons=true&theme=chartreuse-dark)

